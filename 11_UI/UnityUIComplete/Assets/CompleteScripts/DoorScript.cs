﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI; //Namespace added to get to the UI functionality

public class DoorScript : MonoBehaviour
{
    //Public Variables
    public KeyScript doorKey; //Key required to open the door
    public AudioClip doorLocked; //Audio played if door is locked
    public AudioClip doorOpen; //Audio played when door is opened
    public float smooth = 0.5f; //The speed of blend when the door moves

    //Private Variables
    AudioSource _as; //To store object's AudioSource used to play sound
    TriggerScript _ts; //Trigger script for door
    Text _uiText; //Stores UI text that shows the pickUpMesage
    bool doorComplete; //Stores if the door has been opened

    // Start is called before the first frame update
    void Start()
    {
        _uiText = GameObject.Find("PickUpText").GetComponent<Text>(); //Assigns the UI text that shows the pickUpMessage
        _as = GetComponent<AudioSource>(); // Assigns AudioSource
        _ts = GetComponentInChildren<TriggerScript>(); // Assigns TriggerScript from child game object
    }

    //Called every frame
    void Update()
    {
        if (_ts.triggerEntered == true) //Checks to see if the player has entered the trigger vis the TriggerScript
        {
            DoorInteract(); //Runs DoorInteract function once player is in trigger
        }
    }

    //Checks for player input to interact with the door
    void DoorInteract()
    {        
        if (Input.GetKeyDown(KeyCode.E) && doorComplete == false) //Checks for input and whether the door has been used already
        {
            if (doorKey != null) //Checks if the door requires key
            {
                if (doorKey.haveKey == true) //Checks if player has key
                {
                    _uiText.text = "Door Unlocked"; //Sets UI message
                    OpenDoor(); //Runs OpenDoor function
                }
                else //Runs when player does not have key. Door remains locked
                {
                    _uiText.text = "Door Locked"; //Sets UI message
                    _as.clip = doorLocked;
                    _as.Play();
                }
            }
            else //When door does not require key, door opens
            {
                _uiText.text = "Door Opened"; //Sets UI message
                OpenDoor(); //Runs OpenDoor function
            }
        }
    }

    //Opens door
    void OpenDoor()
    {
        StartCoroutine(MoveDoor()); //Starts coroutine to open door
        _as.clip = doorOpen; //Assigns open sound clip to AudioSource
        _as.Play(); //Plays AudioSource
        doorComplete = true; //Stores sate of door completion so it won't run again
        _ts.gameObject.GetComponent<Collider2D>().enabled = false; //Turns of trigger collider
    }

    //Moves door
    IEnumerator MoveDoor()
    {
        Vector3 target = new Vector3(transform.position.x, transform.position.y + 3, 0); //Sets target location of door
        while (Vector3.Distance(transform.position, target) > 0.05f) //Checks to see if door has reached within 0.05 of target location
        {
            transform.position = Vector3.Lerp(transform.position, target, smooth * Time.deltaTime); // Uses lerp to move the door
            yield return null; //Returns to check if door has reached within 0.05 of target location on the next frame
        }
        _uiText.text = ""; //Sets UI message to nothing
        transform.position = target; //Once door is within 0.05 of target location, set door location to the target location
    }
}
