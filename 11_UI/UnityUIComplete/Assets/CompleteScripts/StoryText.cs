﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI; //Namespace added to get to the UI functionality
using UnityEngine.SceneManagement; //Namespace added to get access to SceneManager class

public class StoryText : MonoBehaviour
{
    Text _st; //To store text used to deliver story

    // Start is called before the first frame update
    void Start()
    {
        _st = GameObject.Find("Text_Story").GetComponent<Text>(); //Assigns story text 
        StartCoroutine(Story()); //Starts coroutine that delivers the story
    }

    IEnumerator Story()
    {
        yield return new WaitForSeconds(2); //Pauses for 2 seconds
        _st.text = "The Year is 2333"; //Sets story text line
        yield return new WaitForSeconds(2);//Pauses for 2 seconds
        _st.text = "and bad stuff has happened"; //Sets story text line
        yield return new WaitForSeconds(2);//Pauses for 2 seconds
        SceneManager.LoadScene(1); //Loads next scene
    }
}
