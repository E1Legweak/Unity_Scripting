﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShotgunPickUp : MonoBehaviour
{
    //Private variables
    Collider2D _c; //To store object's collider
    SpriteRenderer _sr; //To store object's SpriteRenderer
    AudioSource _as; //To store object's AudioSource
    Shotgun _sg; //Stores player's Shotgun script
    bool playerEntered = false; //Stores if the trigger has been entered by the player

    // Start is called before the first frame update
    void Start()
    {
        _c = GetComponent<Collider2D>(); //Gets the attached collider
        _sr = GetComponent<SpriteRenderer>(); //Gets the attached SpriteRenderer
        _as = GetComponent<AudioSource>(); //Gets the attached AudioSource
    }

    // Update is called once per frame
    void Update()
    {
        if (playerEntered == true) //Checks to see if playerEntered is true. i.e. is the player in the trigger
        {
            PickUpInteract(); //Runs PickUpInteract function
        }
    }

    //Checks if trigger is entered
    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.tag == "Player") //Checks to see if trigger has been entered by the player by using tags
        {
            playerEntered = true; //Sets playerEntered to true so PickUpInteract is called
            _sg = col.gameObject.GetComponentInChildren<Shotgun>(); //Gets Shotgun component in player controller
        }
    }

    //Checks if trigger is exited
    void OnTriggerExit2D(Collider2D col)
    {
        if (col.tag == "Player") //Checks to see if trigger has been exited by the player by using tags
        {
            playerEntered = false; //Sets playerEntered to false so PickUpInteract is NOT called
            _sg = null; //Empties variable storing Shotgun component
        }
    }

    //Allows player to pick up shotgun
    void PickUpInteract()
    {
        if (Input.GetKeyDown(KeyCode.E)) //Checks for input
        {
            _sg.shotgunPicked = true; //Stores the player having the shotgun in Shotgun component
            _sg.ShotgunActive(); //Turns shotgun on, in Shotgun component
            _sr.enabled = false; //Turns off pickup sprite
            _c.enabled = false; //Turns off collider
            _as.Play(); //Plays pick up sound
        }
    }
}
