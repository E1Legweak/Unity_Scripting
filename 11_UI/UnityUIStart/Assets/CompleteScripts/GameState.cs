﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement; //Namespace added to get access to SceneManager class

public class GameState : MonoBehaviour
{
    //Stores player dead alive state in a static so all scripts can access it without a direct reference
    public static bool playerIsDead = false;

    bool loadStarted = false; //Stores whether load level has started

    // Start is called before the first frame update
    void Start()
    {
        playerIsDead = false; //Ensures playerIsDead is false at the start of each level load
    }

    // Update is called once per frame
    void Update()
    {
        if (playerIsDead == true && loadStarted == false) //Checks to see if player is deed and load level has started.
        {
            Invoke("LoadGame", 5); //Runs load game after a 5 second delay
            loadStarted = true; //Stores that load has started to prevent load from repeatedly being called
        }
    }

    //Loads scene
    void LoadGame()
    {
        SceneManager.LoadScene(0); //Loads scene at index 0 in the projects File>Built Settings
    }
}
