﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GruntEnemy : MonoBehaviour
{
    //Public Variables
    public Transform waypoint1; //Target 1 for enemy path
    public Transform waypoint2; //Target 2 for enemy path
    public float moveSpeed = 10f; // Effects the maximum speed of the player
    public float acceleration = 20f; //Acceleration up to maximum speed

    //Private Variables
    Rigidbody2D _rb; //To store object's  rigidbody   
    Collider2D _c; //To store object's collider
    AudioSource _as; //To store object's AudioSource
    SpriteRenderer _sr; //To store object's SpriteRenderer
    Animator _an; //To store object's Animator
    int direction = 1; //Drirection of travel 1 moves in positive x and -1 moves in negative x
    Transform target; //Stores current target waypoint
    bool isDead; //Stores if the enemy is dead
    bool playerDead; //Stores if the player is dead


    // Start is called before the first frame update
    void Start()
    {
        _rb = GetComponent<Rigidbody2D>(); //Gets the attached rigidbody
        _c = GetComponent<Collider2D>(); //Gets the attached collider
        _as = GetComponent<AudioSource>(); //Gets the attached AudioSource
        _sr = GetComponent<SpriteRenderer>(); //Gets the attached SpriteRenderer
        _an = GetComponent<Animator>(); //Gets the attached Animator
        target = waypoint1; //Sets the target to Waypoint 1 at start of game
        _sr.flipX = true; //Flips the sprite at start of game
    }

    // Update is called once per frame
    void Update()
    {
        if (isDead == false) //Checks if this enemy is still alive
        {
            UpdateTarget(); //If alive, the enemy target updates 
        }
    }

    // FixedUpdate is called once per unit of time (0.02 by default)
    void FixedUpdate()
    {
        if (isDead == false && GameState.playerIsDead == false) //Checks that both the player and this enemy are alive
        {
            Patrol(); //Runs patrol function
        }
        else if (GameState.playerIsDead == true && playerDead == false) //Checks to see if the player has just died
        {
            _rb.velocity = Vector2.zero; //Stops this enemy from moving
            playerDead = true; // Sets playerDead to false so this is not repeatedly called
            _an.enabled = false; //Stops the animation from running
        }
    }

    //Updates the patrol target based upon this enemies proximity to it
    void UpdateTarget()
    {
        if (Mathf.Abs(transform.position.x - target.position.x) < 0.1f) //Checks the absolute distance to the target (absolute means positive value)
        {
            //Once this enemy is near the waypoint
            direction = -direction; //Move direction is flipped
            _sr.flipX = !_sr.flipX; //Sprite is flipped
            if (target == waypoint1)// Checks what the target waypoint is and chooses the other
            {
                target = waypoint2;
            }
            else
            {
                target = waypoint1;
            }
        }
    }

    // Moves this enemy
    void Patrol()
    {
        _rb.AddForce(transform.right * direction * acceleration, ForceMode2D.Force); //Applies force to move
        _rb.velocity = new Vector2(Mathf.Clamp(_rb.velocity.x, -moveSpeed, moveSpeed), _rb.velocity.y); //Caps speed to maximum
    }

    //Called from the PlayerMovement2D script or the Shotgun script if this enemy is killed
    public void Die()
    {
        _as.Play(); // Plays death sound
        _c.enabled = false; //Turns of collider
        isDead = true; //Sets isDead to true to prevent other functionality taking place
        _sr.flipX = !_sr.flipX; //Flips sprite in x
        _sr.flipY = !_sr.flipY; //Flips sprite in y, making it upsidedown
        _an.enabled = false; //Stops animation
        _rb.velocity = new Vector2(_rb.velocity.x, 0); //Stops movment in y axis
        _rb.AddForce(transform.up * 7, ForceMode2D.Impulse); //Applies force in y axis
        Destroy(gameObject, 5); // Destroys this enemy after 5 seconds
    }
}
