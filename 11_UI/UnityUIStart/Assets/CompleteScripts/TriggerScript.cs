﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerScript : MonoBehaviour
{
    //Public variables
    public string triggerTag; //Variable to decide what tag the trigger is looking for
    public bool triggerEntered; //Stores if the trigger has been entered by an object with the triggerTag

    //Private variables

    //Called at game start
    void Start()
    {

    }

    //Checks if trigger is entered
    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.tag == triggerTag) //Compares tag of object entered against the trigger tag variable
        {
            triggerEntered = true; //Stores that the object has entered the trigger
        }
    }

    //Checks if trigger is exited
    void OnTriggerExit2D(Collider2D col)
    {
        if (col.tag == triggerTag)//Compares tag of object entered against the trigger tag variable
        {
            triggerEntered = false; //Stores that the object has exited the trigger
        }
    }
}
