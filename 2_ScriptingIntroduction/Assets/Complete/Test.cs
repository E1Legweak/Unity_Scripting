﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Test : MonoBehaviour
{
    public int value1 = 1;
    public int value2 = 1;

    //This is a comment and it will not be compiled

    /* This is a comment too
     that comments out between the two asterisks
     and slashes*/

    //Comments are used to annotate code

    void Start()
    {
        if (value1 == value2)
        {

        }
    }

    void Update()
    {
        if (Input.anyKey)
        {

        }
    }
}
