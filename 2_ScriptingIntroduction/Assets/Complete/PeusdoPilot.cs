﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PeusdoPilot : MonoBehaviour
{
    float speed = 10; //Speed of player ship
    bool boost = false; //State of boost ability on/off

    // Start is called before the first frame update
    void Start()
    {
        Debug.Log("Time to take off");
    }

    // Update is called once per frame
    void Update()
    {
        Movement();
        Boost();
        Fire();
    }

    //Enables player input to move
    void Movement()
    {
        if (Input.GetKey(KeyCode.A))
        {
            Debug.Log("Move left with a speed of " + speed);
        }
        else if (Input.GetKey(KeyCode.D))
        {
            Debug.Log("Move right with a speed of " + speed);
        }

        if (Input.GetKey(KeyCode.W))
        {
            Debug.Log("Move up with a speed of " + speed);
        }
        else if (Input.GetKey(KeyCode.S))
        {
            Debug.Log("Move down with a speed of " + speed);
        }
    }

    //Enables boost to effect speed 
    void Boost()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (boost == false)
            {
                boost = true;
                speed = 15;
            }
            else
            {
                boost = false;
                speed = 10;
            }
        }
    }

    //Enables weapons to fire
    void Fire()
    {
        if (Input.GetMouseButton(0) && speed < 15)
        {
            Debug.Log("Fire Machine Guns!");
            Debug.Log(Input.mousePosition);
        }

        if (Input.GetMouseButtonUp(1) && speed < 15)
        {
            Debug.Log("Fire Misiles!");
            Debug.Log(Input.mousePosition);
        }
    }
}
