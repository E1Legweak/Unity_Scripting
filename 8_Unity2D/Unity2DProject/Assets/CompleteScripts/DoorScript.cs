﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorScript : MonoBehaviour
{
    public KeyScript doorKey;
    public AudioClip doorLocked;
    public AudioClip doorOpen;

    Collider2D _collider;
    SpriteRenderer _sp;
    AudioSource _as;
    bool doorComplete;
    bool playerEntered;

    // Start is called before the first frame update
    void Start()
    {
        _collider = GetComponent<Collider2D>();
        _sp = GetComponent<SpriteRenderer>();
        _as = GetComponent<AudioSource>();
    }

    void Update()
    {
        if (playerEntered == true)
        {
            DoorInteract();
        }
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.tag == "Player")
        {
            playerEntered = true;
        }
    }

    private void OnTriggerExit2D(Collider2D col)
    {
        if (col.tag == "Player")
        {
            playerEntered = false;
        }
    }

    private void DoorInteract()
    {        
        if (Input.GetKeyDown(KeyCode.E) && doorComplete == false)
        {
            if (doorKey.haveKey == true)
            {
                Debug.Log("OpenDoor");
                _sp.enabled = false;
                _collider.enabled = false;
                _as.clip = doorOpen;
                _as.Play();
                doorComplete = true;
            }
            else
            {
                _as.clip = doorLocked;
                _as.Play();
            }
        }
    }
}
