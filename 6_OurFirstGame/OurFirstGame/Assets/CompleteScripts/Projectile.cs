﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    //public variables 
    public float force = 50; //force bullet is launched with

    //private variables
    Rigidbody _rb; //reference to this game object's rigidbody

    //Awake is called before Start
    void Awake()
    {
        _rb = GetComponent<Rigidbody>(); //asigns rigidbody
    }

    // Start is called before the first frame update
    void Start()
    {
        _rb.AddForce(transform.forward * force, ForceMode.Impulse); //applies force for one frame only
        Destroy(gameObject, 2); //destroys bullet after 2 second
    }

    //Called on collision with another object
    private void OnCollisionEnter()
    {
        //destroys this (the one this script is attached to) game object when it collides with anything
        Destroy(gameObject);
    }
}
