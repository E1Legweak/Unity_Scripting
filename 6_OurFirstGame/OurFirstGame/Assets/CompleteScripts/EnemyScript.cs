﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyScript : MonoBehaviour
{
    //public variables
    public float moveSpeed = 1; //enemy move speed

    //private variables
    Rigidbody _rb; //rigidbody applied to this game object  
    Score _score; //score script applied to GameManager game object

    // Start is called before the first frame update
    void Start()
    {
        _rb = GetComponent<Rigidbody>(); //assigning rigidbody
        moveSpeed = Random.Range(20, 40); //setting random speed
        _score = GameObject.Find("GameManager").GetComponent<Score>(); //Finding the GameManager object and getting the attached Score script to store in local variable
    }

    /*FixedUpdate is called once per specific time interval. Used for continual physics
    applications i.e. continually applying a force*/
    void FixedUpdate()
    {
        //applies force continually 
        _rb.AddForce(transform.forward * moveSpeed, ForceMode.Acceleration);
    }

    //detect collisions with other objects.
    private void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag == "Player")
        {
            _score.PlayerDied();
            Destroy(col.gameObject);
            Destroy(gameObject);
        }
        else if (col.gameObject.tag == "Wall")
        {
            Destroy(gameObject);
        }
        else if (col.gameObject.tag == "Bullet")
        {
            _score.AddScore();
            Destroy(gameObject);
        }
    }
}
