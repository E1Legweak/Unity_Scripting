﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shotgun : MonoBehaviour
{
    public GameObject shot;
    public int numberOfShot;
    public Texture2D reticule;
    public bool shotgunEquiped = false;
    public bool shotgunPicked = false;
    public GameObject shotSpawn;
    public GameObject shotTarget;

    AudioSource _as;
    Rigidbody2D _rb;
    SpriteRenderer _sr;
    Vector2 dir;
    GameObject[] shots;
    Vector3 pen = new Vector3(0, -100, 0);
    LayerMask mask;

    // Start is called before the first frame update
    void Start()
    {
        _sr = GetComponentInChildren<SpriteRenderer>();
        _as = GetComponent<AudioSource>();
        _rb = GetComponentInParent<Rigidbody2D>();

        mask = LayerMask.GetMask("Player");

        if (shotgunPicked == false)
        {
            ShotgunDeActive();
        }
        else
        {
            ShotgunActive();
        }

        SetUpShots();
    }

    void SetUpShots()
    {
        shots = new GameObject[numberOfShot];
        for (int i = 0; i < numberOfShot; i++)
        {
            shots[i] = Instantiate(shot, pen, transform.rotation);
        }
    }

    public void ShotgunActive()
    {
        shotgunEquiped = true;
        _sr.enabled = true;
        Cursor.lockState = CursorLockMode.None;
        Cursor.lockState = CursorLockMode.Confined;
        Cursor.visible = true;
        Cursor.SetCursor(reticule, Vector2.zero, CursorMode.ForceSoftware);
    }

    public void ShotgunDeActive()
    {
        shotgunEquiped = false;
        _sr.enabled = false;
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    void ToggleShotgun()
    {
        if (Input.GetKeyDown(KeyCode.Q))
        {
            if (shotgunEquiped == true)
            {
                ShotgunDeActive();
            }
            else
            {
                ShotgunActive();
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (GameState.playerIsDead == false && shotgunEquiped == true)
        {
            FlipShotgun();
            LookAtMouse();
            GunInput();
        }
        if (shotgunPicked == true)
        {
            ToggleShotgun();
        }
    }

    void FlipShotgun()
    {
        if (transform.position.x < Camera.main.ScreenToWorldPoint(Input.mousePosition).x)
        {
            _sr.flipY = false;
        }
        else
        {
            _sr.flipY = true;
        }
    }

    void LookAtMouse()
    {
        dir = Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position;
        float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
        //Debug.Log(angle);
        transform.rotation = Quaternion.AngleAxis(angle, transform.forward);
    }

    void GunInput()
    {
        if (Input.GetMouseButtonDown(0) && _as.isPlaying == false)
        {
            Fire();
        }
    }

    void Fire()
    {
        Debug.Log("Fire Shotgun!");
        _as.Play();
        _rb.AddForce(dir.normalized * -10, ForceMode2D.Impulse);
        DistributeShots();
        Invoke("PenShots", 1);
    }

    void DistributeShots()
    {
        foreach (GameObject item in shots)
        {
            item.transform.position = shotTarget.transform.position + new Vector3(Random.Range(-1f, 1f), Random.Range(-1f, 1f), 0);
            Vector2 dir = item.transform.position - shotSpawn.transform.position;
            RaycastHit2D hit2D = Physics2D.Raycast(shotSpawn.transform.position, dir, Vector2.Distance(shotSpawn.transform.position, item.transform.position), ~mask);

            //Debug.DrawRay(shotSpawn.transform.position, dir);

            if (hit2D.collider != null)
            {
                item.transform.position = hit2D.point;
                if (hit2D.collider.tag == "Enemy")
                {
                    hit2D.collider.gameObject.BroadcastMessage("Die");
                }
            }
            
        }
    }

    void PenShots()
    {
        foreach (GameObject item in shots)
        {
            item.transform.position = pen;
        }
    }
}
