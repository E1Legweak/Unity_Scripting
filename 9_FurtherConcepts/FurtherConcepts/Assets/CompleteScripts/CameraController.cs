﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public GameObject _player; //Stores player avatar
    public float xSmooth = 1f; //How Smooth the camera follows the player in x
    public float ySmooth = 1f; //How Smooth the camera follows the player in y
    public float camOffsetX = 0; // Amount the camera will be infront of the player
    public float camOffsetY = 0; // Amount the camera will be above or below the player

    float lastPlayerX = 0; // Players position in x on the last frame
    float camTargetX = 0;
    float camX = 0; // The cameras target x value.
    float camY = 0; // The cameras target y value.

    // FixedUpdate is called once per time interval
    void FixedUpdate()
    {
        TrackPlayer();
    }

    void TrackPlayer()
    {
        if (_player != null && GameState.playerIsDead == false) // Checks to see the player exists
        {
            if (lastPlayerX < _player.transform.position.x && _player.transform.position.x - lastPlayerX > 0.1f) // Checks to see if the player has moved significantly right to change the camera's target
            {
                camTargetX = camOffsetX;
            }
            else if(lastPlayerX > _player.transform.position.x && lastPlayerX - _player.transform.position.x > 0.1f) // Checks to see if the player has moved significantly left to change the camera's target
            {
                camTargetX = -camOffsetX;
            }

            camX = Mathf.Lerp(transform.position.x, _player.transform.position.x + camTargetX, xSmooth) ; //Smoothly changes cameras x target
            camY = Mathf.Lerp(transform.position.y, _player.transform.position.y + camOffsetY, ySmooth ); //Smoothly changes cameras y target
            transform.position = new Vector3(camX, camY, -10); //Sets camera position
            lastPlayerX = _player.transform.position.x; //Sets the plays position this frame ready for the next frame
        }
    }
}
