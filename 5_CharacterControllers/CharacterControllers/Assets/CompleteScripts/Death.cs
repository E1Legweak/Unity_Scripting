﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Death : MonoBehaviour
{
    public string deathTag; //tag of object that causes death

    //checks for collisions
    private void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag == deathTag)
        {
            Destroy(gameObject);
        }
    }

    //checks for triggers
    private void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == deathTag)
        {
            Destroy(gameObject);
        }
    }
}
