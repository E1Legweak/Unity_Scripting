﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    public float force = 500; //force bullet is launched with
    Rigidbody _rb; //reference to objects rigidbody

    private void Awake()
    {
        _rb = GetComponent<Rigidbody>(); //asigns rigidbody
    }

    // Start is called before the first frame update
    void Start()
    {
        _rb.AddForce(transform.forward * force, ForceMode.Impulse); //applies force
        Destroy(gameObject, 2); //destroys bullet
    }
}
