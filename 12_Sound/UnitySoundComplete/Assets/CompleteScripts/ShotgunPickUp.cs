﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI; //Namespace added to get to the UI functionality

public class ShotgunPickUp : MonoBehaviour
{
    //Private variables
    Collider2D _c; //To store object's collider
    SpriteRenderer _sr; //To store object's SpriteRenderer
    AudioSource _as; //To store object's AudioSource
    Shotgun _sg; //Stores player's Shotgun script
    Text _uiText; //Stores UI text that shows the pickUpMesage
    Image _si; //Stores UI image that shows shotgun has been aquired
    bool playerEntered = false; //Stores if the trigger has been entered by the player

    // Start is called before the first frame update
    void Start()
    {
        _c = GetComponent<Collider2D>(); //Gets the attached collider
        _sr = GetComponent<SpriteRenderer>(); //Gets the attached SpriteRenderer
        _as = GetComponent<AudioSource>(); //Gets the attached AudioSource
        _uiText = GameObject.Find("PickUpText").GetComponent<Text>(); //Assigns the UI text that shows the pickUpMessage
        _si = GameObject.Find("ShotgunImage").GetComponent<Image>(); //Assigns the UI image that shows the shotgun has been aquired
        _si.enabled = false; //Disables image showing shotgun has been aquired 
    }

    // Update is called once per frame
    void Update()
    {
        if (playerEntered == true) //Checks to see if playerEntered is true. i.e. is the player in the trigger
        {
            PickUpInteract(); //Runs PickUpInteract function
        }
    }

    //Checks if trigger is entered
    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.tag == "Player") //Checks to see if trigger has been entered by the player by using tags
        {
            playerEntered = true; //Sets playerEntered to true so PickUpInteract is called
            _uiText.text = "Press E to Pick Up Shotgun"; //Sets UI message 
            _sg = col.gameObject.GetComponentInChildren<Shotgun>(); //Gets Shotgun component in player controller
        }
    }

    //Checks if trigger is exited
    void OnTriggerExit2D(Collider2D col)
    {
        if (col.tag == "Player") //Checks to see if trigger has been exited by the player by using tags
        {
            playerEntered = false; //Sets playerEntered to false so PickUpInteract is NOT called
            _uiText.text = null; //Sets UI message to nothing
            _sg = null; //Empties variable storing Shotgun component
        }
    }

    //Allows player to pick up shotgun
    void PickUpInteract()
    {
        if (Input.GetKeyDown(KeyCode.E) && GameState.gamePaused == false) //Checks for input
        {
            _sg.shotgunPicked = true; //Stores the player having the shotgun in Shotgun component
            _sg.ShotgunActive(); //Turns shotgun on, in Shotgun component
            _sr.enabled = false; //Turns off pickup sprite
            _c.enabled = false; //Turns off collider
            _uiText.text = null; // Sets UI text to nothing
            _si.enabled = true; //Turns on UI image
            _as.Play(); //Plays pick up sound
        }
    }
}
