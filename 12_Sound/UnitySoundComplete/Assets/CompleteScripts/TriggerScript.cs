﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI; //Namespace added to get to the UI functionality

public class TriggerScript : MonoBehaviour
{
    //Public variables
    public string triggerTag; //Variable to decide what tag the trigger is looking for
    public string pickUpMessage; //The message sent to the UI when trigger is entered
    public bool triggerEntered; //Stores if the trigger has been entered by an object with the triggerTag

    //Private variables
    Text _uiText; //Stores UI text that shows the pickUpMesage

    void Start()
    {
        _uiText = GameObject.Find("PickUpText").GetComponent<Text>(); //Assigns the UI text that shows the pickUpMessage
    }

    //Checks if trigger is entered
    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.tag == triggerTag) //Compares tag of object entered against the trigger tag variable
        {
            _uiText.text = pickUpMessage; //Shows pickUpMessage
            triggerEntered = true; //Stores that the object has entered the trigger
        }
    }

    //Checks if trigger is exited
    void OnTriggerExit2D(Collider2D col)
    {
        if (col.tag == triggerTag)//Compares tag of object entered against the trigger tag variable
        {
            _uiText.text = null;//Gets rid of pickUpMessage
            triggerEntered = false; //Stores that the object has exited the trigger
        }
    }
}
