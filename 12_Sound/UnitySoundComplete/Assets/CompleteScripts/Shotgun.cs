﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shotgun : MonoBehaviour
{
    //Public variables
    public GameObject shot; //To store prefab of shot
    public int numberOfShot; //To decide how many shot the shotgun will fire
    public Texture2D reticule; //Image of reticule
    public bool shotgunEquiped = false; //Stores if the shotgun is equiped
    public bool shotgunPicked = false; //Stores if the shotgun has been aquired
    public GameObject shotSpawn; //The point in space the shot will be fired from
    public GameObject shotTarget; //The point the shot are aimed at

    //Private variables
    AudioSource _as; //To store object's AudioSource
    Rigidbody2D _rb; //To store the parent (the player avatar) object's rigidbody 
    SpriteRenderer _sr; //To store object's SpriteRenderer
    Vector2 dir; //To store the direction fired in
    GameObject[] shots; //An array to store all the shot to be fired
    Vector3 pen = new Vector3(0, -100, 0); //The pen location in which the shot will be used when not fired
    public LayerMask mask; //A layer mask to ignore particular objects with the raycast

    // Start is called before the first frame update
    void Start()
    {
        Cursor.SetCursor(reticule, Vector2.zero, CursorMode.ForceSoftware);
        _sr = GetComponentInChildren<SpriteRenderer>(); //Gets the attached SpriteRenderer
        _as = GetComponent<AudioSource>(); //Gets the attached AudioSource
        _rb = GetComponentInParent<Rigidbody2D>(); //Gets the parent's attached rigidbody

        if (shotgunPicked == false) //Checks if player has shotgun already, if not deactivates it
        {
            ShotgunDeActive(); //Deactivates shotgun
        }
        else
        {
            ShotgunActive(); //Activates shotgun
        }

        SetUpShots(); //Spawns shot and stores them in shots
    }

    //Instantiates shot
    void SetUpShots()
    {
        shots = new GameObject[numberOfShot]; //Recreates shots array
        for (int i = 0; i < numberOfShot; i++) //Loops through shots array and creates and stores a shot at each index
        {
            shots[i] = Instantiate(shot, pen, transform.rotation); //Creates shot
        }
    }

    //Activates shotgun
    public void ShotgunActive()
    {
        shotgunEquiped = true; //Stores equiped state of shotgun
        _sr.enabled = true; //Turns on sprite
        Cursor.lockState = CursorLockMode.None; //Unlocks cursor
        Cursor.lockState = CursorLockMode.Confined; //Confines cursor to game window
        Cursor.visible = true; //Makes cursor visable
        Cursor.SetCursor(reticule, Vector2.zero, CursorMode.ForceSoftware); //Set cursor texture to reticule
    }

    //Deactivates shotgun
    public void ShotgunDeActive()
    {
        shotgunEquiped = false; //Stores equiped state of shotgun
        _sr.enabled = false; //Turns off sprite
        Cursor.lockState = CursorLockMode.Locked; //Locks cursor
        Cursor.visible = false; //Makes cursor invisable
    }

    //Toggles shotgun being equiped
    void ToggleShotgun()
    {
        if (Input.GetKeyDown(KeyCode.Q)) //Checks for player input
        {
            if (shotgunEquiped == true) //Based upon current equiped state, activates or deactivates shotgun
            {
                ShotgunDeActive();
            }
            else
            {
                ShotgunActive();
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (GameState.gamePaused == false)
        {
            if (GameState.playerIsDead == false && shotgunEquiped == true) //Checks that the player is alive and that the shotgun is equiped
            {
                FlipShotgun(); //Calls FlipShotgun
                LookAtMouse(); //Calls LookAtMouse
                GunInput(); //Calls GunInput
            }
            if (shotgunPicked == true)//Checks to see if you have shotgun at all and if you do allows it be equiped and un equiped
            {
                ToggleShotgun();
            }
        }
    }

    //Flips shotgun sprite
    void FlipShotgun()
    {
        if (transform.position.x < Camera.main.ScreenToWorldPoint(Input.mousePosition).x) //Checks which side the player is aiming and flips the shotgun sprite accordingly
        {
            _sr.flipY = false;
        }
        else
        {
            _sr.flipY = true;
        }
    }

    //Rotates the shotgun to look at the mouse cursor
    void LookAtMouse()
    {
        dir = Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position; //Gets the vector between the mouse cursor and the player in world space
        float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg; //Gets the angle between the hypotenuse and adjacent in radians and converts this into degrees
        transform.rotation = Quaternion.AngleAxis(angle, transform.forward); //Rotates the shotgun around its z axis
    }

    //Checks for player gun input
    void GunInput()
    {
        if (Input.GetMouseButtonDown(0) && _as.isPlaying == false) //Checks for gun input and uses length of the sound clip to prevent spamming
        {
            Fire(); //Calls Fire function
        }
    }

    //Fires shotgun
    void Fire()
    {
        _as.Play(); //Plays audio
        _rb.AddForce(dir.normalized * -10, ForceMode2D.Impulse); //Applies force to player in opposite direction to that fired in
        DistributeShots(); //Calls distribute shots
        Invoke("PenShots", 1); //Re pens shots off screen after 1 second
    }

    //Distributes the shot
    void DistributeShots()
    {
        foreach (GameObject item in shots) //Loop through shots array to do the same to each one
        {
            item.transform.position = shotTarget.transform.position + new Vector3(Random.Range(-1f, 1f), Random.Range(-1f, 1f), 0); //Position shot in a radomised position around the shot target
            Vector2 dir = item.transform.position - shotSpawn.transform.position; //Gets the vector between the spawn point and the shot's location
            RaycastHit2D hit2D = Physics2D.Raycast(shotSpawn.transform.position, dir, dir.magnitude, ~mask); //Shoots raycast from spawn point to shot location

            if (hit2D.collider != null) //Checks is raycast hits anything, if not do nothing
            {
                item.transform.position = hit2D.point; //If raycast hits something, re-position shot to this location
                if (hit2D.collider.tag == "Enemy") //If raycast hit enemy, kill it
                {
                    hit2D.collider.gameObject.BroadcastMessage("Die");
                }
            }

        }
    }

    //Moves shot back to pen location off screen
    void PenShots()
    {
        foreach (GameObject item in shots)//Loops though all shots and moves them
        {
            item.transform.position = pen;
        }
    }
}
