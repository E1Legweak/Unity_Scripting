﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    // Start is called before the first frame update
    void Awake()
    {
        GameObject[] objs = GameObject.FindGameObjectsWithTag("MusicManager"); //Finds all objects sharing a tag and stores them in an array

        if (objs.Length > 1) // check if there are more than one object with the tag by checking at the length of the array 
        {
            Destroy(gameObject); //if the length of the array is greater than 1 this instance will be destroyed
        }
        DontDestroyOnLoad(gameObject); //if this is the only instance, this ensures it will persist into the next scene
    }

}
