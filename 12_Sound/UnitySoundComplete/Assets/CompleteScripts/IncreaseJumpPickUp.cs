﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI; //Namespace added to get to the UI functionality

public class IncreaseJumpPickUp : MonoBehaviour
{
    //Public variables
    public int newJumpNumber = 2; //Amount of jumps pickup will enable player to do

    //Private variables
    Collider2D _c; //To store object's collider
    SpriteRenderer _sr; //To store object's SpriteRenderer
    AudioSource _as; //To store object's AudioSource
    PlayerMovement2D _pMove; //Stores player's Player Movement 2D script
    Text _uiText; //Stores UI text that shows the pickUpMesage
    bool playerEntered = false; //Stores if the trigger has been entered by the player

    // Start is called before the first frame update
    void Start()
    {
        _c = GetComponent<Collider2D>(); //Gets the attached collider
        _sr = GetComponent<SpriteRenderer>(); //Gets the attached SpriteRenderer
        _as = GetComponent<AudioSource>(); //Gets the attached AudioSource
        _uiText = GameObject.Find("PickUpText").GetComponent<Text>(); //Assigns the UI text that shows the pickUpMessage
    }

    // Update is called once per frame
    void Update()
    {
        if (playerEntered == true) //Checks to see if playerEntered is true. i.e. is the player in the trigger
        {
            PickUpInteract(); //Runs PickUpInteract function
        }
    }

    //Checks if trigger is entered
    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.tag == "Player") //Checks to see if trigger has been entered by the player by using tags
        {
            playerEntered = true; //sets playerEntered to true so PickUpInteract is called
            _uiText.text = "Press E to Pick Up Double Jump"; // sets UI message 
            _pMove = col.gameObject.GetComponent<PlayerMovement2D>(); // Gets Player Movement 2D component in player controller
        }
    }

    //Checks if trigger is exited
    void OnTriggerExit2D(Collider2D col)
    {
        if (col.tag == "Player") //Checks to see if trigger has been exited by the player by using tags
        {
            _uiText.text = ""; //Sets UI message to nothing
            playerEntered = false;//Sets playerEntered to false so PickUpInteract is NOT called
            _pMove = null; //Empties variable storing Player Movement 2D component
        }
    }

    //Allows player to pick up shotgun
    void PickUpInteract()
    {
        if (Input.GetKeyDown(KeyCode.E) && GameState.gamePaused == false) //Checks for input
        {
            _pMove.numberOfJumps = newJumpNumber; //Sets new amount of jumps
            _sr.enabled = false; //Turns off pickup sprite
            _c.enabled = false; //Turns off collider
            _as.Play(); //Plays pick up sound
            _uiText.text = ""; //Sets UI text to nothing
        }
    }
}
