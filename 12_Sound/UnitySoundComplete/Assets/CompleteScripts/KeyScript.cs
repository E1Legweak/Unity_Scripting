﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI; //Namespace added to get to the UI functionality

public class KeyScript : MonoBehaviour
{
    //Public variables
    public bool haveKey = false; //Stores whether the key has been collected

    //Private variables
    Collider2D _c; //To store object's  collider
    SpriteRenderer _sr; //To store object's SpriteRenderer
    AudioSource _as; //To store object's AudioSource
    Text _uiText; //Stores UI text that shows the pickUpMesage
    bool playerEntered; //Stores if the trigger has been entered by the player

    // Start is called before the first frame update
    void Start()
    {
        _c = GetComponent<Collider2D>(); //Gets the attached collider
        _sr = GetComponent<SpriteRenderer>(); //Gets the attached SpriteRenderer
        _as = GetComponent<AudioSource>(); //Gets the attached AudioSource
        _uiText = GameObject.Find("PickUpText").GetComponent<Text>(); //Assigns the UI text that shows the pickUpMessage
    }

    // Update is called once per frame
    void Update()
    {
        if (playerEntered == true) //Checks to see if playerEntered is true. i.e. is the player in the trigger
        {
            PickUpInteract(); //Runs PickUpInteract function
        }
    }

    //Checks if trigger is entered
    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.tag == "Player") //Checks to see if trigger has been entered by the player by using tags
        {
            playerEntered = true; //sets playerEntered to true so PickUpInteract is called
            _uiText.text = "Press E to Pick Up Key"; // sets UI message 
        }
    }

    //Checks if trigger is exited
    void OnTriggerExit2D(Collider2D col)
    {
        if (col.tag == "Player") // Checks to see if trigger has been exited by the player by using tags
         {
            playerEntered = false; //sets playerEntered to false so PickUpInteract is NOT called
            _uiText.text = ""; // sets UI message to nothing
        }
    }

    //Allows player to pick up key
    void PickUpInteract()
    {
        if (Input.GetKeyDown(KeyCode.E) && GameState.gamePaused == false) //Checks for input
        {
            haveKey = true; //Stores the player having the key 
            _sr.enabled = false; //Turns off sprite
            _c.enabled = false; // Turns off collider
            _uiText.text = ""; // Sets UI text to nothing
            _as.Play(); // Plays pick up sound
        }
    }
}
