﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio; //Namespace added to get access to Audio 
using UnityEngine.SceneManagement; //Namespace added to get access to SceneManager 

public class GameState : MonoBehaviour
{
    //public variables available in inspector
    public GameObject pauseMenu; //to store pause menu game object
    public AudioMixer allMixer; //to store our audio mixer
    public AudioMixerSnapshot paused; //to store paused audio snapshot
    public AudioMixerSnapshot play; //to store play audio snapshot

    //Stores player dead alive state in a static so all scripts can access it without a direct reference
    public static bool playerIsDead = false;
    public static bool gamePaused = false;

    bool loadStarted = false; //Stores whether load level has started
    bool menuOpen = false; //Stores menu state
    bool cursorVisible = false; //Stores cursor current visiblity
    CursorLockMode currentMode; //Stores cursor current mode

    // Start is called before the first frame update
    void Start()
    {
        playerIsDead = false; //Ensures playerIsDead is false at the start of each level load
        gamePaused = false; //Ensure the state of the game is not paused at begining of level
        pauseMenu.SetActive(false); //Turns of pause menu
    }

    // Update is called once per frame
    void Update()
    {
        if (playerIsDead == true && loadStarted == false) //Checks to see if player is deed and load level has started.
        {
            Invoke("LoadGame", 5); //Runs load game after a 5 second delay
            loadStarted = true; //Stores that load has started to prevent load from repeatedly being called
        }
        else
        {
            PauseMenu(); //Whilst player is alive runs the PauseMenu function
        }
    }

    //Checks for input and opens the menu when player presses P
    void PauseMenu()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            //checks current state of menu
            if (menuOpen == true)
            {
                pauseMenu.SetActive(false); //Turns off pause menu
                menuOpen = false; //Sets state of pause menu to off 
                gamePaused = false; //Sets state static variabel to not paused 
                Time.timeScale = 1; //Sets time scale to 1
                play.TransitionTo(0.5f); //Transitions to snapshot play
                Cursor.lockState = currentMode; //Sets cursor lock state back to what it was
                Cursor.lockState = CursorLockMode.Confined; //Confines cursor to game screen
                Cursor.visible = cursorVisible; //Sets cursor visibility back to what it was
            }
            else
            {
                pauseMenu.SetActive(true); //Turns on pause menu
                menuOpen = true; //Sets state of pause menu to on
                gamePaused = true; //Sets state static variabel to paused 
                paused.TransitionTo(0); //Transitions to snapshot paused
                Time.timeScale = 0; //Sets time scale to o
                cursorVisible = Cursor.visible; //Gets the current cursor visibility
                currentMode = Cursor.lockState; //Gets the current cursor lock state
                Cursor.lockState = CursorLockMode.None; //Sets cursor lock state back to none
                Cursor.lockState = CursorLockMode.Confined; //Confines cursor to game screen
                Cursor.visible = true; //Sets cursor visibility back to on
            }
        }
    }

    //Loads scene
    void LoadGame()
    {
        SceneManager.LoadScene(1); //Loads scene at index 1 in the projects File>Built Settings
    }

    //Quits the game from Pause Menu
    public void QuitGame()
    {
        Application.Quit();
    }

    //Enables the setting of the FX Volume from Pause Menu
    public void FXLevel(float fxLv)
    {
        allMixer.SetFloat("fxVol", fxLv);
    }

    //Enables the setting of the Music Volume from Pause Menu
    public void MusicLevel(float musicLv)
    {
        allMixer.SetFloat("musicVol", musicLv);
    }
}
