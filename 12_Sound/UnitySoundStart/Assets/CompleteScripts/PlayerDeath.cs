﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDeath : MonoBehaviour
{
    //Public variables
    public AudioClip deathHit;
    public AudioClip deathDrop;

    //Private variables
    Rigidbody2D _rb; //To store object's rigidbody
    Collider2D _c; //To store object's collider
    PlayerMovement2D _pm2D; //To store object's Player Movement 2D script   
    AudioSource _as; //To store object's AudioSource

    // Start is called before the first frame update
    void Start()
    {
        _rb = GetComponent<Rigidbody2D>(); //Gets the attached rigidbody
        _c = GetComponent<Collider2D>(); //Gets the attached collider
        _pm2D = GetComponent<PlayerMovement2D>(); //Gets the attached Player Movement 2D script
        _as = GetComponent<AudioSource>(); //Gets the attached AudioSource
    }

    //Kills the player. Public so it can be called from another script
    public void Die()
    {
        StartCoroutine(PlayerDrop()); //Starts coroutine to show player death
    }

    IEnumerator PlayerDrop()
    {
        GameState.playerIsDead = true; //Set static variable in GameState that the player is dead
        _c.enabled = false; //Turns off collider
        _pm2D.enabled = false; //Turns off Player Movement 2D script
        _rb.velocity = Vector2.zero; //Stops playr from moving
        _rb.gravityScale = 0; //Ensures player does not fall
        _as.clip = deathHit; //Sets AudioSource clip to death hit sound
        _as.Play(); //Plays death hit sound

        yield return new WaitForSeconds(1f); //Pauses for 1 second
        _rb.gravityScale = 2; //Turns gravity back on
        _rb.AddForce(transform.up * 7, ForceMode2D.Impulse); //Applies upwards force to player
        _as.clip = deathDrop; //Sets AudioSource clip to death drop sound
        _as.Play(); //Plays death drop sound
    }
}
