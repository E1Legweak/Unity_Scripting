﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AimCannon : MonoBehaviour
{
    // Update is called once per frame
    void Update()
    {
        Rotate();
    }

    void Rotate()
    {
        if (Input.GetKey(KeyCode.W))
        {
            transform.Rotate(new Vector3 (0,0,-45) * Time.deltaTime);
        }
        else if (Input.GetKey(KeyCode.S))
        {
            transform.Rotate(new Vector3(0, 0, 45) * Time.deltaTime);
        }
    }
}
