﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerEvent : MonoBehaviour
{
    AudioSource _as;
    bool eventTriggered;
    Throw _throw;
    
    // Start is called before the first frame update
    void Start()
    {
        _as = GetComponent<AudioSource>();
        _throw = GameObject.Find("Scare").GetComponent<Throw>();
    }

    private void OnTriggerEnter(Collider col)
    {
        if (col.tag == "Player" && eventTriggered == false)
        {
            Debug.Log("Player entered");
            _as.Play();
            _throw.ScareEvent();
            eventTriggered = true;
        }
    }
}
