﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour
{
    public float acceleration = 1;
    public float maxSpeed = 1;

    Rigidbody2D _rb;
    float movement;

    // Start is called before the first frame update
    void Start()
    {
        _rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        MoveInput();
    }

    void FixedUpdate()
    {
        Move(); //Move is called here as it applies force continuously
    }

    void MoveInput()
    {
        movement = Input.GetAxis("Horizontal");
    }

    void Move()
    {
        _rb.AddForce(transform.right * movement * acceleration, ForceMode2D.Force); //Applies force to move
        _rb.velocity = new Vector2(Mathf.Clamp(_rb.velocity.x, -maxSpeed, maxSpeed), _rb.velocity.y); //Caps speed to maximum
    }
}
