﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerJump : MonoBehaviour
{
    [SerializeField]
    private float jumpHeight = 10;

    Rigidbody2D _rb;
    PlayerCollisions _pc;

    // Start is called before the first frame update
    void Start()
    {
        _rb = GetComponent<Rigidbody2D>();
        _pc = GetComponent<PlayerCollisions>();
    }

    // Update is called once per frame
    void Update()
    {
        JumpInput();
    }

    void JumpInput()
    {
        if (Input.GetButtonDown("Jump"))
        {
            if (_pc.isGrounded == true)
            {
                _rb.AddForce(transform.up * jumpHeight, ForceMode2D.Impulse);
            }
        }
    }
}
