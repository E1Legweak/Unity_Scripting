﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputObserver : MonoBehaviour
{
    public delegate void InputAction();
    public static event InputAction OnInput;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            if (OnInput != null)
            {
                OnInput();
            }
        }
    }
}
