﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Encapsulated : MonoBehaviour
{
    [SerializeField]
    private float speed;

    public float Speed
    {
        get
        {
            return speed;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
