﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OutputObserver : MonoBehaviour
{
    private void OnEnable()
    {
        InputObserver.OnInput += ConsoleMessage;
    }

    private void OnDisable()
    {
        InputObserver.OnInput -= ConsoleMessage;
    }

    private void ConsoleMessage()
    {
        Debug.Log("Input received");
    }
}
