﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxForce : MonoBehaviour
{
    //public variables
    public Rigidbody _rb; // assign in inspector

    //private variables
    float boxSpeed = 2.5f;
    BoxSpawner boxSpawner;

    // Start is called before the first frame update
    void Start()
    {
        //assigns box spawner
        boxSpawner = GameObject.FindGameObjectWithTag("BoxSpawner").GetComponent<BoxSpawner>();
    }

    // Update is called once per frame
    void Update()
    {
        //applies force to box
        _rb.AddForce(transform.forward * boxSpeed, ForceMode.Force);
    }

    // Called when entering trigger
    private void OnTriggerEnter(Collider other)
    {
        //destroys box at exit point
        if (other.gameObject.tag == "BoxExitPoint")
        {
            boxSpawner.storedBoxes++;
            Debug.Log("We have stored atotal of: " + boxSpawner.storedBoxes + " boxes.");
            Destroy(gameObject, 0.1f);
        }
    }
}
