﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxSpawner : MonoBehaviour
{
    //public variables 
    public GameObject blueBox; //prefab to spawn
    public GameObject yellowBox; //prefab to spawn
    public GameObject spawnPoint; //spawn point

    //private variables
    int blueBoxesSpawned; //number of blue boxes spawned
    int yellowBoxesSpawned; //number of yellow boxes spawned
    public int storedBoxes; //number of total boxes reached finish
    bool allowBoxSpawning; //state of conveyer

    // Start is called before the first frame update
    void Start()
    {
        blueBoxesSpawned = 0;
        yellowBoxesSpawned = 0;
        storedBoxes = 0;
        allowBoxSpawning = false;
    }

    // Update is called once per frame
    void Update()
    {
        ConveyerBeltState();
        Spawn();
    }

    //Turns conveyer on and off
    void ConveyerBeltState()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            if (allowBoxSpawning == true)
            {
                allowBoxSpawning = false;
            }
            else
            {
                allowBoxSpawning = true;
            }
            Debug.Log("The conveyor belt is enabled: " + allowBoxSpawning);
        }
    }

    //Spawns box if conveyer is on
    void Spawn()
    {
        if (allowBoxSpawning)
        {
            if (Input.GetKeyDown(KeyCode.Alpha1))
            {
                blueBoxesSpawned++;
                Debug.Log("Amountof blue boxes spawned is: " + blueBoxesSpawned);
                Instantiate(blueBox, spawnPoint.transform.position, spawnPoint.transform.rotation);
            }
            if (Input.GetKeyDown(KeyCode.Alpha2))
            {
                yellowBoxesSpawned++;
                Debug.Log("Amountof yellow boxes spawned is: " + yellowBoxesSpawned);
                Instantiate(yellowBox, spawnPoint.transform.position, spawnPoint.transform.rotation);
            }
        }
    }
}
