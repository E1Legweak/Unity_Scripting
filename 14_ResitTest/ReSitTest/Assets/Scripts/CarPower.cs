﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarPower : MonoBehaviour
{
    Rigidbody _rb;
    float acceleration;
    bool raceStart;

    // Start is called before the first frame update
    void Start()
    {
        acceleration = Random.Range(1f, 20f);
        _rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        RaceStart();
        
    }

    private void FixedUpdate()
    {
        CarAcceleration();
    }

    void RaceStart()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            raceStart = true;
        }
    }

    void CarAcceleration()
    {
        if (raceStart)
        {
            if (acceleration < 20)
            {
                acceleration = acceleration + 0.1f;
            }
            _rb.AddForce(transform.forward * acceleration, ForceMode.Acceleration);
        }
    }
}
