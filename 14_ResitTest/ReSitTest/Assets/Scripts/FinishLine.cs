﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinishLine : MonoBehaviour
{
    RaceManager raceManager;

    // Start is called before the first frame update
    void Start()
    {
        raceManager = GameObject.Find("StartLine").GetComponent<RaceManager>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "CarRed")
        {
            raceManager.takings = raceManager.takings + 10000;
            raceManager.numberOfCarsFinished++;
            raceManager.redCarActive = false;
            Destroy(other.gameObject, 2);
        }
        else if (other.gameObject.tag == "CarBlue")
        {
            raceManager.takings = raceManager.takings + 20000;
            raceManager.numberOfCarsFinished++;
            raceManager.blueCarActive = false;
            Destroy(other.gameObject, 2);
        }
        Debug.Log(raceManager.numberOfCarsFinished + " cars have raced today. The bookmakers have made £" + raceManager.takings + " in bets");
    }
}
