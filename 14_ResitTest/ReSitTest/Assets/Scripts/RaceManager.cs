﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RaceManager : MonoBehaviour
{
    GameObject spawnPoint1;
    GameObject spawnPoint2;

    public GameObject redCar;
    public GameObject blueCar;

    bool raceOpen;
    public int numberOfCarsFinished;
    public float takings;

    public bool redCarActive;
    public bool blueCarActive;

    // Start is called before the first frame update
    void Start()
    {
        spawnPoint1 = GameObject.FindGameObjectWithTag("SpawnPoint1");
        spawnPoint2 = GameObject.FindGameObjectWithTag("SpawnPoint2");
    }

    // Update is called once per frame
    void Update()
    {
        OpenRace();
        SpawnCars();
    }

    void OpenRace()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            raceOpen = !raceOpen;
            Debug.Log(raceOpen);
            if (!raceOpen)
            {
                takings = 0;
                numberOfCarsFinished = 0;
            }
        }
    }

    void SpawnCars()
    {
        if (raceOpen)
        {
            if (Input.GetKeyDown(KeyCode.Alpha1) && !redCarActive)
            {
                redCarActive = true;
                Debug.Log("Red Car is ready to Race");
                Instantiate(redCar, spawnPoint1.transform.position, spawnPoint1.transform.rotation);
            }
            if (Input.GetKeyDown(KeyCode.Alpha2) && !blueCarActive)
            {
                blueCarActive = true;
                Debug.Log("Blue Car is ready to Race");
                Instantiate(blueCar, spawnPoint2.transform.position, spawnPoint2.transform.rotation);
            }
        }
    }
}
