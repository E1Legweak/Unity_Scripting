﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameState : MonoBehaviour
{
    public static bool playerIsDead = false;
    bool loadStarted = false;

    // Start is called before the first frame update
    void Start()
    {
        playerIsDead = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (playerIsDead == true && loadStarted == false)
        {
            Invoke("LoadGame", 5);
            loadStarted = true;
        }
    }

    void LoadGame()
    {
        SceneManager.LoadScene(0);
    }
}
