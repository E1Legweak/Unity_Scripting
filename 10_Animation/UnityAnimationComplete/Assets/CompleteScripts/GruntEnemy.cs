﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GruntEnemy : MonoBehaviour
{
    public Transform waypoint1;
    public Transform waypoint2;
    public float moveSpeed = 10f; // Effects the maximum speed of the player
    public float acceleration = 20f;

    Rigidbody2D _rb;
    Collider2D _c;
    AudioSource _as;
    SpriteRenderer _sr;
    Animator _an;
    int direction = 1;
    Transform target;
    bool isDead;
    bool playerDead;


    // Start is called before the first frame update
    void Start()
    {
        _rb = GetComponent<Rigidbody2D>();
        _c = GetComponent<Collider2D>();
        _as = GetComponent<AudioSource>();
        _sr = GetComponent<SpriteRenderer>();
        _an = GetComponent<Animator>();
        target = waypoint1;
        _sr.flipX = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (isDead == false)
        {
            UpdateTarget();
        }
    }

    private void FixedUpdate()
    {
        if (isDead == false && GameState.playerIsDead == false)
        {
            Patrol();
        }
        else if (GameState.playerIsDead == true && playerDead == false)
        {
            _rb.velocity = Vector2.zero;
            playerDead = true;
            _an.enabled = false;
        }
    }

    void UpdateTarget()
    {
        if (Mathf.Abs(transform.position.x - target.position.x) < 0.1f)
        {
            direction = -direction;
            _sr.flipX = !_sr.flipX;
            if (target == waypoint1)
            {
                target = waypoint2;
            }
            else
            {
                target = waypoint1;
            }
        }
    }

    void Patrol()
    {
        _rb.AddForce(transform.right * direction * acceleration, ForceMode2D.Force); //Applies force to move
        _rb.velocity = new Vector2(Mathf.Clamp(_rb.velocity.x, -moveSpeed, moveSpeed), _rb.velocity.y); //Caps speed to maximum
    }

    public void Die()
    {
        _as.Play();
        _c.enabled = false;
        isDead = true;
        _sr.flipX = !_sr.flipX;
        _sr.flipY = !_sr.flipY;
        _an.enabled = false;
        _rb.velocity = new Vector2(_rb.velocity.x, 0);
        _rb.AddForce(transform.up * 7, ForceMode2D.Impulse);
        Destroy(gameObject, 5);
    }
}
