﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerScript : MonoBehaviour
{
    public string triggerTag;
    public bool triggerEntered;

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.tag == triggerTag)
        {
            triggerEntered = true;
        }
    }

    private void OnTriggerExit2D(Collider2D col)
    {
        if (col.tag == triggerTag)
        {
            triggerEntered = false;
        }
    }
}
